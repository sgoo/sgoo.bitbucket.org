setTimeout(() => {
    const timing = performance.timing;

    const start = timing.fetchStart;

    const end = timing.loadEventEnd;

    console.debug('took: ' + (end - start));
}, 1000);

document.addEventListener("DOMContentLoaded", function(event) {
    const q = window.location.search;
    const start = q.indexOf('body=') + 5;
    document.querySelector('h1').textContent += q.substring(start, q.indexOf('&', start));

    AP.resize();
});
