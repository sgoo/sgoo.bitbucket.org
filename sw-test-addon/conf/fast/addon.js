navigator.serviceWorker.register('sw.js').then(function(registration) {
    // Registration was successful
    console.debug('ServiceWorker registration successful with scope: ', registration.scope);
}).catch(function(err) {
    // registration failed :(
    console.debug('ServiceWorker registration failed: ', err);
});

setTimeout(() => {
    const timing = performance.timing;

    const start = timing.fetchStart;

    const end = timing.loadEventEnd;

    console.debug('took: ' + (end - start));
}, 1000);

window.addEventListener("load", function(event) {
    console.debug('content loaded');
    const q = window.location.search;
    const start = q.indexOf('body=') + 5;
    const h1 = document.querySelector('h1');
    h1.textContent += q.substring(start, q.indexOf('&', start));

    console.debug('setting resize to', h1.clientWidth, h1.clientHeight);
    AP.resize(h1.clientWidth, h1.clientHeight);
});
