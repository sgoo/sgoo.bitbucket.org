var CACHE_NAME = 'my-site-cache-v4';
var urlsToCache = [
    'addon.js',
    'page-macro.html',
];


self.addEventListener('install', function(event) {
    console.debug("Installed", event);
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.debug('Opened cache ', CACHE_NAME);
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('fetch', function(event) {
    const fetchStart = performance.now();
    console.debug('checking fetch: ' + event.request.url);
    event.respondWith((async() => {
        const cacheResponse = await caches.match(event.request, { ignoreSearch: true });
        if (cacheResponse) {
            console.debug('returing from cache, took: ' + (performance.now() - fetchStart));
            return cacheResponse;
        }


        const fetchRequest = event.request.clone();
        const fetchResponse = await fetch(fetchRequest);

        console.debug('got response type: ' + fetchResponse.type);

        // Check if we received a valid response
        if(!fetchResponse || fetchResponse.status !== 200 || fetchResponse.type !== 'basic') {
            console.debug('returning uncachable response');
            return fetchResponse;
        }

        const responseToCache = fetchResponse.clone();
        caches.open(CACHE_NAME)
            .then(cache => cache.put(event.request, responseToCache));

        console.debug('returning new cached response');
        return fetchResponse;
    })());
});